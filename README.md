anime-tracker
---

A silly little command line tool to track anime progress into spreadsheets.

```
usage: anime-tracker.py [-h] [-a ADD | -l | -r REMOVE] [-c] [-e EPISODE]
                        [-f FILE] [-s SECONDARY]

Track anime progress to spreadsheet

optional arguments:
  -h, --help            show this help message and exit
  -a ADD, --add ADD     Add anime to list. Also needed to edit anime.
  -l, --list            List out all anime on the list
  -r REMOVE, --remove REMOVE
                        Remove anime from list
  -c, --complete        Set the status of progress to complete.
  -e EPISODE, --episode EPISODE
                        Set the episode count for a specific title.
  -f FILE, --file FILE  Specify a spreadsheet file to work with. New fle is
                        created if omitted.
  -s SECONDARY, --secondary SECONDARY
                        Set secondary name for anime.
```

## Modules Used
- openpyxl
- argparse
- os

#### Use -h flag to display help

