#!/bin/python

import os
import argparse
from openpyxl.workbook import Workbook
from openpyxl import load_workbook
from openpyxl.styles import Font

parser = argparse.ArgumentParser(description='Track anime progress to spreadsheet')

arg_group = parser.add_mutually_exclusive_group()
arg_group.add_argument('-a', '--add', help='Add anime to list. Also needed to edit anime.')
arg_group.add_argument('-l', '--list', action='store_true', help='List out all anime on the list')
arg_group.add_argument('-r', '--remove', help='Remove anime from list')

parser.add_argument('-c', '--complete', action='store_true', help='Set the status of progress to complete.')
parser.add_argument('-e', '--episode', help='Set the episode count for a specific title.')
parser.add_argument('-f', '--file', help='Specify a spreadsheet file to work with. New fle is created if omitted.')
parser.add_argument('-s', '--secondary', help='Set secondary name for anime.')

args = parser.parse_args()

def anime(anime):
	check = False
	row = 0
	for cell in ws['A']:
		if cell.value == anime:
			check = True
			row = cell.row
		else:
			pass
			
	if check == True:
		check_flags(row)

	else:
		rows = ws.max_row
		last_row = rows + 1
		ws.cell(row=last_row, column=1, value=anime)
		wb.save(filename = filename)
		check_flags(last_row)

def check_flags(row):
	# if secondary flag(-s) is used
	if args.secondary:
		ws.cell(row=row, column=2, value=args.secondary)
		wb.save(filename = filename)
	else: 
		...

	# if complete flag(-c) is used
	if args.complete:
		ws.cell(row=row, column=4, value = "True")
		wb.save(filename = filename)
	else:
		...

	# if episode flag(-e) is used
	if args.episode:
		ws.cell(row=row, column=3, value=args.episode)
		wb.save(filename = filename)
	else:
		...

def create_spreadsheet():
	global wb
	global ws
	global filename

	wb = Workbook()
	ws = wb.active
	ws.title = 'anime-tracker'

    # setup formatting
	ws.column_dimensions['A'].width = 60
	ws.column_dimensions['B'].width = 60
	ws.column_dimensions['C'].width = 10
	ws.column_dimensions['D'].width = 10
	ws['A1'] = 'Name'
	ws['A1'].font = Font(bold=True, underline='single')
	ws['B1'] = 'Secondary Name'
	ws['B1'].font = Font(bold=True, underline='single')
	ws['C1'] = 'Episode'
	ws['C1'].font = Font(bold=True, underline='single')
	ws['D1'] = 'Complete'
	ws['D1'].font = Font(bold=True, underline='single')

	# check if a file exists already, revisit to make it better
	if os.path.isfile('anime-tracker.xlsx'):
		filename = 'anime-tracker-1.xlsx'
	else:
		filename = 'anime-tracker.xlsx'

	wb.save(filename = filename)

def load_spreadsheet():
    global wb
    global ws
    global filename

    wb = load_workbook(args.file)
    ws = wb.active

    filename = args.file

def list_anime():
	# iterate through rows, printing out each cell in the row
	for row in ws.iter_rows():
		for cell in row:
			if cell.value == 'Name' or cell.value == 'Episode':
				pass
			else:
				print(cell.value)
		print('---')

def remove_anime(anime):
	for cell in ws['A']:
		if cell.value == anime:
			row = cell.row
			ws.delete_rows(row)
			wb.save(filename = filename)

		else:
			pass

# check for file
if args.file:
	load_spreadsheet()
	ws = wb.active

else:
	create_spreadsheet()

# add or "select" anime
if args.add:
	anime(args.add)

# remove anime
elif args.remove:
	remove_anime(args.remove)

# list anime and details
elif args.list:
	list_anime()

# complain
else:
	print('what do????')